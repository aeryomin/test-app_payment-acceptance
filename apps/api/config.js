const options = {
  serverPort: process.env.NX_SERVER_PORT,
  clientPort: process.env.NX_CLIENT_DEV_PORT,
  app: process.env.NX_APP,
  env: process.env.NX_NODE_ENV,
  mongoURL: process.env.NX_MONGO_URL,
}

export default options
