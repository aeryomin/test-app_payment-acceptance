export interface IPayment {
    cardNumber: number;
    expDate: Date;
    cvv: number;
    amount: number;
  __v: number;
  _id: string;
}

export type NewPaymentType = Omit<IPayment, '__v' | '_id'>;

