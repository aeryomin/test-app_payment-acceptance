import { Router } from 'express';

import * as paymentController from '../../controller/payment.controller';

const router = Router();

router.post('/makePayment', paymentController.makePayment);

export default router;
