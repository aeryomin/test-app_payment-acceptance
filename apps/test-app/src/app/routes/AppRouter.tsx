import React from 'react';

import {
    BrowserRouter,
    Route,
    Switch,
} from 'react-router-dom';

import { IndexRoute } from '.';
import { LandingPage } from '../modules/LandingPage';

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={IndexRoute.url()} component={LandingPage} />
            </Switch>
        </BrowserRouter>
    );
};
