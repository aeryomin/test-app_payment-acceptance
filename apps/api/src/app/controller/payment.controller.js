import { PaymentService } from '../services/payment.service';

export async function makePayment(req, res) {
    try {
        const response = await PaymentService.makePayment(req.body);

        res.json({ RequestId: response._id, Amount: response.amount });
    } catch (err) {
        res.json({ status: 'error', err });
    }
}
