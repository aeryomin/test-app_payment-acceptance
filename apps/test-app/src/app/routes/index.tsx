export class IndexRoute {
    public static url() {
        return '/';
    }
}

export class AdminRoute {
    public static url() {
        return '/admin';
    }
}

export class UserRoute {
    public static url() {
        return '/user/:uid';
    }
}
