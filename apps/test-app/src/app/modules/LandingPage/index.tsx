import React from 'react';

import { Form, Input, Button, message } from 'antd';

import { NewPaymentType } from '@test-app-nx/api-interfaces';

import { makePayment } from '../../api';
import { getExpirationDate } from '../../utils/getExpirationDate';
import { validators } from '../../utils/validators';

import css from './index.module.css';

interface IFormData {
    cardNumber: number;
    expDate: string;
    cvv: number;
    amount: number;
}

export const LandingPage = () => {
    const [form] = Form.useForm<IFormData>();

    const onFinish = (formData: IFormData) => {
        const [month, year] = formData.expDate.split('/');
        const expDate = getExpirationDate(Number(month), Number(year));

        const data: NewPaymentType = {
            ...formData,
            expDate,
        };

        const response = makePayment(data);
        response.then(res => {
            message.success(
                `Request ID: ${res.data.RequestId} Amount: ${res.data.Amount}`,
                2,
            );
        });
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className={css.landingPageWrap}>
            <Form
                form={form}
                className={css.form}
                name="cardForm"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="Card Number"
                    name="cardNumber"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter card number',
                        },
                        () => ({
                            validator(_, value) {
                                return validators.cardNumberValidator(value);
                            },
                        }),
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Expiration Date"
                    name="expDate"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter Expiration Date',
                        },
                        () => ({
                            validator(_, value) {
                                return validators.expirationDateValidator(
                                    value,
                                );
                            },
                        }),
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="CVV"
                    name="cvv"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter CVV',
                        },
                        () => ({
                            validator(_, value) {
                                return validators.CVVValidator(value);
                            },
                        }),
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Amount"
                    name="amount"
                    rules={[
                        {
                            required: true,
                            message: 'Please enter Amount',
                        },
                        () => ({
                            validator(_, value) {
                                return validators.amountValidator(value);
                            },
                        }),
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8, span: 16 }} shouldUpdate>
                    {() => (
                        <Button
                            type="primary"
                            htmlType="submit"
                            disabled={
                                !form.isFieldsTouched(true) ||
                                !!form
                                    .getFieldsError()
                                    .filter(({ errors }) => errors.length)
                                    .length
                            }
                        >
                            Оплатить
                        </Button>
                    )}
                </Form.Item>
            </Form>
        </div>
    );
};
