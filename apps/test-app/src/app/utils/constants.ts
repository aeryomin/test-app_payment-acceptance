export const SERVER_URL = `http://localhost:${process.env.NX_SERVER_PORT}`;

export const CARD_NUMBER_LENGTH = 16;
export const DATE_FORMAT = 'MM/YYYY';
export const CVV_LENGTH = 3;
