export function getExpirationDate(month: number, year: number) {
    const lastDayInMonth = new Date(year, month, 0).getDate();
    return new Date(year, month - 1, lastDayInMonth);
}
