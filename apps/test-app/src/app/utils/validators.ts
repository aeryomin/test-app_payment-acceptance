import { CARD_NUMBER_LENGTH, DATE_FORMAT, CVV_LENGTH } from './constants';

export const validators = {
    cardNumberValidator,
    expirationDateValidator,
    CVVValidator: CvvValidator,
    amountValidator,
};

function cardNumberValidator(value) {
    if (!value) {
        return Promise.reject();
    }
    if (isNaN(value)) {
        return Promise.reject('Card number has to be a number.');
    }
    if (value.length !== CARD_NUMBER_LENGTH) {
        return Promise.reject(
            `Card number must be ${CARD_NUMBER_LENGTH} digits`,
        );
    }
    return Promise.resolve();
}

function expirationDateValidator(value) {
    if (!value) {
        return Promise.reject();
    }
    if (!/^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/.test(value)) {
        return Promise.reject(`Date format is ${DATE_FORMAT}`);
    }
    return Promise.resolve();
}

function CvvValidator(value) {
    if (!value) {
        return Promise.reject();
    }
    if (isNaN(value)) {
        return Promise.reject('CVV has to be a number.');
    }
    if (value.length !== CVV_LENGTH) {
        return Promise.reject(`CVV must be ${CVV_LENGTH} digits`);
    }
    return Promise.resolve();
}

function amountValidator(value) {
    if (!value) {
        return Promise.reject();
    }
    if (isNaN(value)) {
        return Promise.reject('Amount has to be a number.');
    }
    return Promise.resolve();
}
