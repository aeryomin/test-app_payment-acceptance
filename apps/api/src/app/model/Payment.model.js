import mongoose from 'mongoose';

const PaymentsSchema = new mongoose.Schema({
  cardNumber: {
    type: Number,
    required: true,
  },
  expDate: {
    type: Date,
    required: true,
  },
  cvv: {
    type: Number,
    required: true,
  },
  amount: {
    type: Number,
    required: false,
  },
});

export default mongoose.model('payments', PaymentsSchema);
