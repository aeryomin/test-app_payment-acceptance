import axios from 'axios';

import { NewPaymentType } from '@test-app-nx/api-interfaces';

const server_port = process.env.NX_SERVER_PORT

const baseURL = `http://localhost:${server_port}`;

const api = axios.create({ baseURL });

export const makePayment = ({
    cardNumber,
    expDate,
    cvv,
    amount,
}: NewPaymentType) => {
    return api.post('api/v1/payment/makePayment', {
        cardNumber,
        expDate,
        cvv,
        amount,
    });
};
