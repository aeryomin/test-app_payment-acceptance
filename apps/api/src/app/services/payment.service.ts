import { IPayment } from '@test-app-nx/api-interfaces';

import Payments from '../model/Payment.model';

export class PaymentService {
    static async makePayment(data: IPayment) {
        const payment = new Payments(data);

        const response: IPayment = await payment.save();

        return response;
    }
}
