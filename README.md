

# Test task for the DataSub

  - Install deps by `npm install`
  - Rename `.env-example` to `.env`
  - Run Server `npm run server:start`
  - Run Client `npm run client:start`
  - Run both `npm run dev`
