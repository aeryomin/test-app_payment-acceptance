/* eslint-disable import/order */
import cors from 'cors';
import express from 'express';
import config from '../config';
import paymentRoutes from './app/routes/api/payment.routes';
import { connect as mongooseServiceConnect } from './app/services/mongoose';

mongooseServiceConnect();

const PORT = config.serverPort || 8090;

const app = express();

const middleware = [
    cors(),
    express.urlencoded({
        limit: '50mb',
        extended: true,
        parameterLimit: 50000,
    }),
    express.json({ limit: '50mb', type: 'application/json' }),
];

middleware.forEach(it => app.use(it));

app.use('/api/v1/payment/', paymentRoutes);

const server = app.listen(PORT, () => {
    console.log('Listening at http://localhost:' + PORT + '/api/v1');
});

server.on('error', console.error);
